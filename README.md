
This is the base install that runs the containerized Discourse setup for <https://forum.f-droid.org>:

https://f-droid.org/docs/Maintaining_the_Forum/


## Deployment to remote server

You simply need to run the playbook from this project to bootstrap
fdroid server to a remote host.

First you'll need to install ansible, on your local machine e.g.:

    sudo apt install ansible

You'll also need to make sure that the bare minimum for ansible
is installed on your remote host:

    sudo apt install python3-apt sudo

Fetch ansible dependencies:

    ansible-galaxy install -f -r requirements.yml -p .galaxy

Then you can immediately start the deployment:

    ansible-playbook -i remoteHost, provision.yml
